import { Component, OnInit, ViewChild, ContentChild, ContentChildren, QueryList } from "@angular/core";
import { TabComponent } from "../tab/tab.component";
import { TabsNavComponent } from '../tabs-nav/tabs-nav.component';
import { TabDirective } from '../tab.directive';

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.scss"]
})
export class TabsComponent implements OnInit {

  @ContentChild(TabsNavComponent)
  navRef:TabsNavComponent

  @ContentChildren(TabDirective)
  tabsList:QueryList<TabDirective>

  active: TabDirective;

  ngAfterContentInit(){
    this.navRef.tabs = this.tabsList
    
    this.tabsList.forEach(tab=>{

      tab.activeChange.subscribe((tab)=>{
        this.toggle(tab)
      })

    })
  }

  toggle(tab: TabDirective) {
    this.active = tab;
    
    this.tabsList.forEach(tab => {
        tab.active = tab == this.active;
    });
  }
  

  ngOnInit() {
  }


  constructor() {}

}
