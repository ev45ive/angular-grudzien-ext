import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export abstract class AuthConfig {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token = "";

  constructor(private config: AuthConfig) {}

  authorize() {
    const { auth_url, redirect_uri, response_type, client_id } = this.config;

    const p = new HttpParams({
      fromObject: {
        redirect_uri, //:redirect_uri,
        response_type,
        client_id
      }
    });
    sessionStorage.removeItem('token')
    location.replace(`${auth_url}?${p.toString()}`);
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token"));

    if (!this.token && location.hash) {
      const p = new HttpParams({
        fromString: location.hash
      });
      this.token = p.get("#access_token");
      location.hash = "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
