import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormArray,
  AbstractControl,
  FormBuilder,
  Validators,
  ValidatorFn,
  AsyncValidatorFn,
  Validator,
  ValidationErrors
} from "@angular/forms";
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  @Input()
  set query(q: string) {
    (this.queryForm.get("query") as FormControl).setValue(q, {
      // onlySelf: true,
      emitEvent: false,
      // emitViewToModelChange:false,
      // emitModelToViewChange: false
    });
  }

  constructor() {
    const censor: ValidatorFn = (
      control: AbstractControl
    ): ValidationErrors | null => {
      const hasError = (control.value as string).includes("batman");

      return hasError
        ? {
            censor: { badword: "batman" }
          }
        : null;
    };

    const asyncCensor: AsyncValidatorFn = (
      control: AbstractControl
    ): Observable<ValidationErrors | null> => {
      // return this.http.post('/validat',valie).pipe(map(resp => errors))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handler = setTimeout(() => {
            const hasError = (control.value as string).includes("batman");
            const result = hasError ? { censor: { badword: "batman" } } : null;

            observer.next(result);
            observer.complete();
          }, 2000);

          // onUnsubscribe
          return () => {
            clearTimeout(handler);
          };
        }
      );
    };

    this.queryForm = new FormGroup({
      query: new FormControl(
        "",
        [
          Validators.required,
          Validators.minLength(3)
          // censor
        ],
        [asyncCensor]
      )
    });

    const field = this.queryForm.get("query");
    const value$ = field.valueChanges;
    const status$ = field.statusChanges;

    const valid$ = status$.pipe(filter(status => status == "VALID"));

    const search$ = valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    );

    search$
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(query => this.search(query));
  }

  @Output()
  queryChange = new EventEmitter();

  search(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
