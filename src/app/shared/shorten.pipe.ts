import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  // pure: false
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, toLength = 20): any {
    // console.log('znowu!')

    return value.substr(0,toLength);
    // return Math.random()
  }

}
