import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HighlightDirective } from "./highlight.directive";
import { UnlessDirective } from "./unless.directive";
import { CardComponent } from "./card/card.component";
import { TabsComponent } from "./tabs/tabs.component";
import { TabComponent } from "./tab/tab.component";
import { TabsNavComponent } from "./tabs-nav/tabs-nav.component";
import { TabDirective } from "./tab.directive";
import { ShortenDirective } from "./shorten.directive";
import { ShortenPipe } from "./shorten.pipe";
import { RouteMessageComponent } from './route-message/route-message.component';
import { ItemsListComponent } from '../playlists/components/items-list/items-list.component';

@NgModule({
  declarations: [
    HighlightDirective,
    UnlessDirective,
    CardComponent,
    TabsComponent,
    TabComponent,
    TabsNavComponent,
    TabDirective,
    ShortenDirective,
    ShortenPipe,
    RouteMessageComponent,
    ItemsListComponent
  ],
  imports: [CommonModule],
  exports: [
    HighlightDirective,
    UnlessDirective,
    CardComponent,
    TabsComponent,
    TabComponent,
    TabsNavComponent,
    TabDirective,
    ShortenDirective,
    ShortenPipe,
    ItemsListComponent
  ]
})
export class SharedModule {}
