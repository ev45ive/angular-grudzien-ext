import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TestingService } from "../testing.service";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.component.html",
  styleUrls: ["./testing.component.scss"]
})
export class TestingComponent implements OnInit {
  
  @Input()
  message = "testing works!";
  response = "";

  save(message: string) {
    this.messageChange.emit(message)
    
    this.service.sendMessage(message)
    .subscribe(resp => {
      this.response = resp;
    });
  }

  @Output()
  messageChange = new EventEmitter()

  constructor(private service: TestingService) {}

  ngOnInit() {}
}
