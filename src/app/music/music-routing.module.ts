import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchComponent } from "./components/music-search/music-search.component";
import { SelectedAlbumComponent } from "./containers/selected-album/selected-album.component";
import { MusicService } from "./services/music.service";

const routes: Routes = [
  {
    path: "",
    component: MusicSearchComponent
  },
  {
    path: "album/:id",
    resolve: {
      album: MusicService
    },
    component: SelectedAlbumComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule {}
