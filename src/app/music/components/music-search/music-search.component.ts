import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicService } from "../../services/music.service";
import { Subscription, Subject, ConnectableObservable } from "rxjs";
import {
  takeUntil,
  tap,
  multicast,
  refCount,
  shareReplay,
  share
} from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
  // viewProviders:[
  //   MusicService
  // ],
})
export class MusicSearchComponent implements OnInit {
  albums$ = this.service.getAlbums().pipe(
    tap(albums => (this.albums = albums)),
    share()
  );

  query$ = this.service.getQueries();

  albums: Album[];
  message: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicService
  ) {}

  search(query: string) {
    this.service.search(query);

    this.router.navigate(['./'], {
      queryParams: {
        q: query
      },
      replaceUrl:true,
      relativeTo: this.route
    });
  }

  sub: Subscription;

  ngOnInit() {
    this.sub = this.route.queryParamMap.subscribe(paramMap => {
      const q = paramMap.get("q");

      if (q !== null) {
        this.search(q);
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
