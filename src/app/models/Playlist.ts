export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * HEX Color
   */
  color: string;
  // tracks: Array<Track>;
  tracks?: Track[]
}

export interface Track {
  id: number;
  name: string;
  preview_url:string;
}

/* 
class Point{
  x: number; //y:number;
}

interface Vector{
   x: number;  y:number;
}

type X = Point | Vector

let p1:X
let v1:Vector

p1 = v1 
v1 = p1 */
