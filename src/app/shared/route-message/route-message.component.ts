import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-route-message',
  templateUrl: './route-message.component.html',
  styleUrls: ['./route-message.component.scss']
})
export class RouteMessageComponent implements OnInit {

  message$ = this.route.data.pipe(
    map( data => data['message'])
  )

  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
  }

}
