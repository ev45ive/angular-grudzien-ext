import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import { TestingComponent } from "./testing.component";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { TestingService } from "../testing.service";
import { Subject, empty } from "rxjs";

describe("TestingComponent", () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;
  let serviceSpy: jasmine.SpyObj<TestingService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestingComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: TestingService,
          useValue: jasmine.createSpyObj("TestingService", ["sendMessage"])
          // useClass: TestingService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    serviceSpy = TestBed.get(TestingService);
    serviceSpy.sendMessage.and.returnValue(empty());
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render message", () => {
    expect(fixture.nativeElement.innerHTML).toMatch("testing works!");
  });

  it("should render updated message", () => {
    component.message = "Placki";
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css("p.message"));

    expect(elem.nativeElement.textContent).toMatch("Placki");
  });

  it("should render message in text input", () => {
    const elem = fixture.debugElement.query(By.css("input.message-field"));

    return fixture.whenStable().then(() => {
      expect(elem.nativeElement.value).toBe(component.message);
    });
  });

  it("changing input should update message", () => {
    const elem = fixture.debugElement.query(By.css("input.message-field"));

    // elem.nativeElement.value = 'Placki'
    // elem.nativeElement.dispatchEvent(new Event('input'))

    // lub:

    elem.triggerEventHandler("input", {
      target: {
        value: "Placki"
      }
    });

    expect(component.message).toBe("Placki");
  });

  it("should call save() with message when save button is pressed", () => {
    const elem = fixture.debugElement.query(By.css(".save-button"));
    const spy = spyOn(component, "save");

    elem.triggerEventHandler("click", {});

    expect(spy).toHaveBeenCalledWith(component.message);
  });

  it("should send message to service when save() is called", inject(
    [TestingService],
    (service: jasmine.SpyObj<TestingService>) => {
      component.save("Placki");

      expect(service.sendMessage).toHaveBeenCalledWith("Placki");
    }
  ));

  it("should display response from service ", inject(
    [TestingService],
    (service: jasmine.SpyObj<TestingService>) => {
      const subject = new Subject();

      service.sendMessage.and.returnValue(subject.asObservable());

      component.save("Placki?");

      subject.next("Tak, poprosze!");
      expect(component.response).toEqual("Tak, poprosze!");
    }
  ));

  it("should emit event when saving", () => {
    component.messageChange.subscribe(message => {
      expect(message).toEqual("placki");
    });
    component.save("placki");
  });
});
