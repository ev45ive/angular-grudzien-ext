import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./components/playlists-view/playlists-view.component";
import { SelectedPlaylistComponent } from "./containers/selected-playlist/selected-playlist.component";
import { PlaylistsListComponent } from "./containers/playlists-list/playlists-list.component";
import { RouteMessageComponent } from "../shared/route-message/route-message.component";
import { PlaylistResolverService } from './resolvers/playlist.service';

const routes: Routes = [
  {
    path: "playlists",
    component: PlaylistsViewComponent,
    // states: Selected or not
    children: [
      {
        // Nothing selected View
        path: "",
        children: [
          {
            path: "",
            component: PlaylistsListComponent,
            outlet: "list"
          },
          {
            path: "",
            component: RouteMessageComponent,
            data: {
              placki: "Awesome",
              message: "Please select playlist"
            }
          }
        ]
      },
      {
        // Playlist selected View
        path: ":id",
        resolve:{
          playlist: PlaylistResolverService
        },
        children: [
          {
            path: "",
            component: PlaylistsListComponent,
            outlet: "list"
          },
          {
            path: "",
            component: SelectedPlaylistComponent,
          }
        ]
      }
    ]
  }
  // {
  //   path: "playlists/:id",
  //   component: PlaylistsViewComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
