import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.scss"]
})
export class TabComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  open = false;

  @Output()
  activeChange = new EventEmitter()

  toggle() {
    this.activeChange.emit(this)
  }

  constructor() {
    
  }

  ngOnInit() {}
}
