import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from '../../services/playlists.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: []
})
export class PlaylistsListComponent implements OnInit {
  
  playlists$ = this.service.getPlaylists();
  selected$ = this.service.getSelected()

  constructor(
    private router: Router,
    private service: PlaylistsService
  ) {}

  select(id: number) {
    this.router.navigate(["/playlists", id]);
  }

  ngOnInit() {
    this.service.fetchPlaylists();
  }

}
