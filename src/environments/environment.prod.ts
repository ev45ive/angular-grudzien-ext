import { AuthConfig } from "../app/security/auth.service";

export const environment = {
  production: true,
  api_url: "https://api.spotify.com/v1/search",

  authConfig: {
    auth_url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "4a0a2c95ae82420bbdda3c06a22acbc1"
  } as AuthConfig
};
