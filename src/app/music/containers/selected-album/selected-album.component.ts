import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { pluck } from "rxjs/operators";
import { Album } from '../../../models/Album';
import { Track } from '../../../models/Playlist';

@Component({
  selector: "app-selected-album",
  templateUrl: "./selected-album.component.html",
  styleUrls: ["./selected-album.component.scss"]
})
export class SelectedAlbumComponent implements OnInit {

  selected:Track

  album$ = this.route.data.pipe(
    pluck<{}, Album>("album")
  );

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {}
}
