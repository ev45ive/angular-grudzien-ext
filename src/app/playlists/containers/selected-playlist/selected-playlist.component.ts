import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PlaylistsService } from "../../services/playlists.service";
import { Playlist } from "../../../models/Playlist";
import { map } from 'rxjs/operators';

@Component({
  selector: "app-selected-playlist",
  templateUrl: "./selected-playlist.component.html",
  styleUrls: []
})
export class SelectedPlaylistComponent implements OnInit {
  
  selected$ = this.route.data.pipe(
    map(data => data['playlist'] as Playlist)
  )

  constructor(
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  save(draft: Playlist) {
    this.service.savePlaylist(draft);
  }

  ngOnInit() {  }
}
