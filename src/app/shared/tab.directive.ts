import { Directive, Input, TemplateRef, ViewContainerRef, EventEmitter } from '@angular/core';
import { element } from 'protractor';

@Directive({
  selector: '[appTab]'
})
export class TabDirective {

  @Input('appTab')
  title

  activeChange = new EventEmitter()

  toggle(){
    this.activeChange.emit(this)
  }

  open

  set active(active){
    this.open = active;
    
    if(active){
      if(this.vcr.length == 0){
        this.vcr.createEmbeddedView(this.tpl)
      }
    }else{
      this.vcr.clear()
    }
  }

  constructor(
    private tpl:TemplateRef<any>,
    private vcr: ViewContainerRef
  ) { }

}
