import { NgModule } from "@angular/core";
import {
  Routes,
  RouterModule,
  
  PreloadAllModules} from "@angular/router";
import { PlaylistsListComponent } from './playlists/containers/playlists-list/playlists-list.component';

const routes: Routes = [
  {
    path: "",
    // component: HomeComponent
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "music",
    loadChildren:'./music/music.module#MusicModule',
    // children:[
    //   {
    //     path: "",
    //     component: MusicSearchComponent
    //   }
    // ]
  },
  {
    path:'placki',
    component: PlaylistsListComponent,
    pathMatch:'full',
    outlet:'popup'
  },
  {
    path: "**",
    // component: PageNotFoundComponent
    redirectTo: "playlists"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true,
      // useHash: true,
      // preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
