import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "../../models/Album";
import { HttpClient } from "@angular/common/http";
import {
  map,
  merge,
  concat,
  startWith,
  switchAll,
  switchMap,
  catchError
} from "rxjs/operators";
import {
  of,
  Subject,
  AsyncSubject,
  ReplaySubject,
  BehaviorSubject,
  empty
} from "rxjs";
import { query } from "@angular/core/src/render3";
import { ActivatedRouteSnapshot } from '@angular/router';

export const SEARCH_URL = new InjectionToken("Search API Url");

@Injectable({
  providedIn: "root"
})
export class MusicService {
  errorChange = new Subject<Error>();
  albumsChange = new BehaviorSubject<Album[]>([]);
  queryChange = new BehaviorSubject<string>("batman");
  // queryChange = new ReplaySubject<string>(1);

  constructor(
    @Inject(SEARCH_URL)
    private search_url: string,
    private http: HttpClient
  ) {
    this.queryChange
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params => this.fetchAlbums(params)),
        // switchAll(),
        map(resp => resp.albums.items)
      )
      .subscribe(albums => this.albumsChange.next(albums));
  }

  fetchAlbums(params: { type: string; q: string }) {
    return this.http.get<AlbumsResponse>(this.search_url, { params }).pipe(
      catchError(err => {
        this.errorChange.next(err);
        return empty();
      })
    );
  }

  fetchAlbum(id: string) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${id}`);
  }

  resolve(route:ActivatedRouteSnapshot){
    return this.fetchAlbum(route.paramMap.get('id'))
  }

  /* Inputs / Commands */
  search(query: string) {
    this.queryChange.next(query);
  }

  /* Outputs / Queries */
  getAlbums() {
    return this.albumsChange.asObservable();
  }

  getQueries() {
    return this.queryChange.asObservable();
  }
  // public queries$ = this.queryChange.asObservable()

  ngOnDestroy() {
    // console.log('Service says bye bye!')
  }
}
