import { Track } from './Playlist';

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists?: Artist[];
  tracks?: PagingObject<Track>
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
  total?: number;
  offset?: number;
}

export interface AlbumsResponse{
  albums: PagingObject<Album>
}