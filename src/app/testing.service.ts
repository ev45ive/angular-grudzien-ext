import { Injectable, Inject, InjectionToken } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient } from '@angular/common/http';

export const API_URL = new InjectionToken('api url');

@Injectable({
  providedIn: "root"
})
export class TestingService {

  sendMessage(message: string): Observable<any> {
    
    return this.http.get(this.api_url,{
      params:{
        q:message
      }
    })
  }

  constructor(
    @Inject(API_URL)
    private api_url:string,
    private http:HttpClient) {}
}
