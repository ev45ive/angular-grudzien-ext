import { TestBed } from '@angular/core/testing';

import { PlaylistResolverService } from './playlist.service';

describe('PlaylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaylistResolverService = TestBed.get(PlaylistResolverService);
    expect(service).toBeTruthy();
  });
});
