import { Directive } from "@angular/core";
import { MusicService } from "./services/music.service";

@Directive({
  selector: "[appSearchProvider]",
  providers: [
    MusicService,
    // {
    //   provide: SomeMusicSearchService,
    //   useClass: SpotifyMusicService
    // }
  ]
})
export class SearchProviderDirective {
  constructor() {}
}
