import { TestBed } from "@angular/core/testing";

import { TestingService, API_URL } from "./testing.service";
import { HttpClientModule } from "@angular/common/http";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

fdescribe("TestingService", () => {
  let service: TestingService;
  let httpController: HttpTestingController;

  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: API_URL,
          useValue: "fake_url.com"
        }
      ],
      imports: [HttpClientTestingModule]
    })
  );
  beforeEach(() => {
    service = TestBed.get(TestingService);
    httpController = TestBed.get(HttpTestingController);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should send message to server", () => {
    service.sendMessage("Placki").subscribe(resp => {
      expect(resp).toEqual("Tak, poprosze");
    });

    const req = httpController.expectOne({
      method: "GET",
      url: "fake_url.com?q=Placki"
    });
    req.flush("Tak, poprosze");

    httpController.verify();
  });
});
