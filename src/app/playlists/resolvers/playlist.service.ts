import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Playlist } from "../../models/Playlist";
import { Observable } from "rxjs";
import { PlaylistsService } from "../services/playlists.service";
import { skip, take, delay } from "rxjs/operators";


@Injectable({
  providedIn: "root"
})
export class PlaylistResolverService implements Resolve<Playlist> {
  constructor(private service: PlaylistsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Playlist> {

    const id = parseInt(route.paramMap.get("id"));
    this.service.select(id);

    return this.service.getSelected().pipe(
      take(1),
      delay(2000)
    );
  }
}
