import {
  Directive,
  ElementRef,
  Attribute,
  Input,
  OnInit,
  DoCheck,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host: {
  //   "[style.color]": "color",
  //   "(mouseenter)":'activate($event)'
  // }
})
export class HighlightDirective {
  @Input("appHighlight")
  color;

  @HostBinding("style.border-left-color")
  @HostBinding("style.color")
  get currentColor() {
    return this.active ? this.color : "";
  }

  constructor() {}

  active = false;

  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x: number, y: number) {
    this.active = true;
  }

  @HostListener("mouseleave", [])
  deactivate() {
    this.active = false;
  }

  /* 
  ngOnChanges(changes: SimpleChanges) {
    // console.log("ngOnChanges", changes);
    // this.elem.nativeElement.style.color = this.color;
  }

  ngOnInit() {
    console.log("ngOnInit", this.color);
  }
  
  ngDoCheck() {
    console.log("ngDoCheck");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy");
  } */
}
