import { Directive, TemplateRef, ViewContainerRef, ComponentFactoryResolver, Input, ViewRef } from "@angular/core";
import { PlaylistsViewComponent } from '../playlists/components/playlists-view/playlists-view.component';

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {

  viewCache:ViewRef

  @Input()
  set appUnless(hide){
    if(hide){
      // this.vcr.clear()
      this.viewCache = this.vcr.detach(0)
    }else{
      if(this.viewCache){
        this.vcr.insert(this.viewCache)
      }else{
        this.vcr.createEmbeddedView(this.tpl)
      }
    }
  }

  constructor(
    private tpl:TemplateRef<any>,
    private vcr:ViewContainerRef
  ) {

    
    
  }
}
