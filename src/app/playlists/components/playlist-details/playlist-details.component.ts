import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input() playlist: Playlist;

  constructor() {}

  ngOnInit() {}

  mode = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @ViewChild("formRef" /* , { read: NgForm } */)
  form: NgForm;


  @Output()
  playlistChange = new EventEmitter<Playlist>()

  save(form: NgForm) {
    const draft:Partial<Playlist> = form.value;
    

    const playlist = {
      ...this.playlist,
      ...draft
    };
    // console.log(form)

    this.playlistChange.emit(playlist)
    this.mode = 'show'
  }
}

// type Partial<T> = {
//   [key in keyof T]?: T[key]
// }
