import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchComponent } from "./components/music-search/music-search.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { AlbumsGridComponent } from "./components/albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_URL, MusicService } from "./services/music.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';
import { SearchProviderDirective } from './search-provider.directive';
import { SharedModule } from '../shared/shared.module';
import { SelectedAlbumComponent } from './containers/selected-album/selected-album.component';

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent,
    SearchProviderDirective,
    SelectedAlbumComponent
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    // HttpClientModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [SearchProviderDirective],
  ///
  providers: [
    {
      provide: SEARCH_URL,
      useValue: environment.api_url
    },
    // {
    //   provide: MusicService,
    //   useFactory: (url:string) => {
    //     return new MusicService(url)
    //   },
    //   deps:[SEARCH_URL]
    // },
    // {
    //   provide: AbstractMusicService,
    //   useClass: ConcreteMusicService,
    //   // deps: [SEARCH_URL]
    // },
    MusicService,
  ]
})
export class MusicModule {}
