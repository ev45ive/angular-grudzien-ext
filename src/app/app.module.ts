import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";

import { SecurityModule } from './security/security.module';
import { TestingComponent } from './testing/testing.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent, TestingComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    PlaylistsModule,
    // MusicModule,
    SharedModule,
    SecurityModule,
    AppRoutingModule,
  ],
  providers: [],
  // entryComponents:[AppComponent]
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app:ApplicationRef){}
  // ngDoBootstrap() {
  //   this.app.bootstrap(AppComponent,'app-root')
  // }
}
