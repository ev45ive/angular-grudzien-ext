import { Injectable } from "@angular/core";
import { Playlist } from "../../models/Playlist";
import { Observable, of, Subject, BehaviorSubject, ReplaySubject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { tap, switchMap, distinctUntilChanged, share } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private _playlists = new BehaviorSubject<Playlist[]>([]);
  private _selectedId = new ReplaySubject<Playlist["id"]>(1);

  selected = this._selectedId.pipe(
    switchMap(id => this.getPlaylist(id)),
    share()
  );

  private api_url = "http://localhost:3000/playlists/";

  constructor(private http: HttpClient) {}

  /* Commands - Inputs: */

  fetchPlaylists() {
    this.http.get<Playlist[]>(this.api_url)
    .subscribe(playlists => {
      this._playlists.next(playlists);
    });
  }

  select(playlistId: Playlist["id"]) {
    this._selectedId.next(playlistId);
  }

  savePlaylist(playlist: Playlist) {
    this.http
      .put<Playlist>(this.api_url + playlist.id, playlist)
      .pipe(
        tap(() => this.fetchPlaylists()),
        tap(() => this.select(playlist.id))
      )
      .subscribe();
  }

  /* Queries - Outputs: */

  getSelected() {
    return this.selected;
  }

  getPlaylists(): Observable<Playlist[]> {
    return this._playlists.asObservable();
  }

  getPlaylist(id: number): Observable<Playlist> {
    return this.http.get<Playlist>(this.api_url + id);
  }
}
